#MainMenu.gd
# Código para el menú principal

extends Control

# Esto son `señales´ (una funcion de Godot para vincular otros nodos a un script)

# Cuando se presiona el nodo StartButton (que es un botón)
func _on_StartButton_pressed():
	# Se cambia la escena a la del archivo del primer mundo
	get_tree().change_scene("Worlds/World.tscn")

# Cuando se presiona el nodo QuitButton
func _on_QuitButton_pressed():
	# Se cierra el programa
	get_tree().quit()
