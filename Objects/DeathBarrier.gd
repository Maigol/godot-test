#DeathBarrier.gd
# Código para la bárra de muerte debajo de cada nivel

extends Area2D

# Esta función se ejecuta cada frame
func _physics_process(delta):
	# Por cada `cuerpo` dentro de la barra
	for body in get_overlapping_bodies():
		# Si el nombre del cuerpo es Player
		if body.name == "Player":
			# Morir
			die()

# Función de morir
func die():
	# Recargar la escena
	get_tree().reload_current_scene()
