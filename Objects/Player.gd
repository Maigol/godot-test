extends KinematicBody2D

# Variables y constantes
const GRAVITY = 20
const TERMINAL_GRAV = 1500
const SPEED_X_MAX = 300
const FRICTION_GROUND = .2
const FRICTION_AIR = .05
const FRICTION_WALL = 125
const ACCELERATION = 30
const JUMP_HEIGHT = -600
const WALLJUMP = abs(JUMP_HEIGHT)/2
const WALL_MARGIN_OUT = 30
const WALL_MARGIN_IN = 35
const UP = Vector2(0, -1)
const BULLET = preload("res://objects/Bullet.tscn")
var motion = Vector2()
var facing = 1
var canBuffer = false
var jumpBuffer = false
onready var Sprite = $Sprite
onready var WallMargin = $WallMargin
onready var JumpMargin = $JumpMargin
var canShoot = true

# Aplica la gravedad
func applyGravity():
	# Elige la mínima entre la velocidad vertical actual + GRAVITY
	#  y TERMINAL_GRAV, que es la velocidad terminal de caída.
	motion.y = min(motion.y+GRAVITY, TERMINAL_GRAV)

# Maneja el movimiento
func manageMovement():
	var friction = false
	
	# Movimiento horizontal
	if Input.is_action_pressed("right"): # Derecha
		# Escoge la opción mínima entre la velocidad + aceleración
		#  o la velocidad máxima. 
		motion.x = min(motion.x+ACCELERATION, SPEED_X_MAX)
	elif Input.is_action_pressed("left"): #Izquierda
		# Lo mismo que lo anterior pero al revéz (porque la velocidad es negativa
		#  hacia la izquierda)
		motion.x = max(motion.x-ACCELERATION, -SPEED_X_MAX)
	else: # Quieto
		friction = true
		
	# Disparos
	if Input.is_action_just_pressed("Shoot") && canShoot:
		# Crear una nueva instancia de Shoot y hacerla hija del nivel
		var bullet = BULLET.instance()
		get_parent().add_child(bullet)
		
		# Posicionarla sobre el arma
		bullet.position = $Gun.global_position
		
		# Iniciar el cooldown
		canShoot = false
		$ShootCooldown.start()

	# Aplicar fricción
	if friction:
		# Aplica interpolación lineal desde la velocidad actual
		#  hasta velocidad 0 tan rápido como el 3er argumento indique. 
		motion.x = lerp(motion.x, 0, FRICTION_GROUND)
	else:
		if friction:
			motion.x = lerp(motion.x, 0, FRICTION_AIR)
	# Fricción con el muro
	if $StateMachine.state == $StateMachine.states.wallslide and motion.y > 0:
		motion.y = lerp(motion.y, FRICTION_WALL, 1)
	
	# Voltear sprite (flip_h) dependiendo de `facing` (hacia dónde se mira)
	#  (hay un caso especial al hacer wallslide, por eso no se voltea en ese estado)
	if not $StateMachine.state == $StateMachine.states.wallslide:
		if Input.is_action_pressed("right"):
			facing = 1
			WallMargin.cast_to.x = WALL_MARGIN_OUT
			Sprite.flip_h = false
		elif Input.is_action_pressed("left"):
			facing = -1
			WallMargin.cast_to.x = -WALL_MARGIN_OUT
			Sprite.flip_h = true
	
	# Aplicar movimiento
	# Reasignar `motion` previene que la gravedad se sume para siempre
	#  (Probá a ver qué le passa a `motion.y` si quitás `motion = `)
	motion = move_and_slide(motion, UP)

func jump():
	motion.y = JUMP_HEIGHT

func showDebug():
	# Recargar escena con `reset`
	if Input.is_action_pressed("reset"):
		return get_tree().reload_current_scene()
	
	$Sprite/StateLabel.text = $StateMachine.states.keys()[$StateMachine.state]
	
	var DebugLabel = $Camera2D/CanvasLayer/DebugLabel
	
	# Mostrar alguans variables (Como los FPS) en DebugLabel
	var fps = str(Engine.get_frames_per_second())
	DebugLabel.text = "FPS: "+fps
	DebugLabel.text += "\nMotion: "+str(motion)
	DebugLabel.text += "\nJumpBuffer: "+str(jumpBuffer)
	DebugLabel.text += "\nCan buffer: "+str(canBuffer)

func canWallslide():
	if WallMargin.is_colliding() != false:
		if WallMargin.get_collider().name == "TileMap":
			WallMargin.cast_to.x = WALL_MARGIN_IN if facing > 0 else -WALL_MARGIN_IN
			return true
	WallMargin.cast_to.x = WALL_MARGIN_OUT if facing > 0 else -WALL_MARGIN_OUT
	return false


func _on_ShootCooldown_timeout():
	canShoot = true
