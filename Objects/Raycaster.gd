extends Node2D

const UP = Vector2(0, -1)
const DOWN = Vector2(0, 1)
const LEFT = Vector2(-1, 0)
const RIGHT = Vector2(1, 0)
const SKIN_WIDTH = 1

export(int) var bufferSize = 3 setget _setBufferSize
var buffer = []

func _ready():
	_setBufferSize(bufferSize)

func raycast(direction, rect, mask, exceptions = [], rayLength = 16, buffer = self.buffer):
	if not [UP, DOWN, LEFT, RIGHT].has(direction):
		return 0
	
	var space_state = get_world_2d().direct_space_state
	var extents = rect.extents - Vector2(SKIN_WIDTH, SKIN_WIDTH)
	var count = 0
	var rayCount = buffer.size()
	var castTo = (rayLength + SKIN_WIDTH) * direction
	var origin
	var spacing
	
	if [UP, DOWN].has(direction):
		spacing = extents.x * 2 / (rayCount - 1)
	else:
		spacing = extents.y * 2 / (rayCount - 1)
	
	for i in range(rayCount):
		if [UP, DOWN].has(direction):
			origin = Vector2(-extents.x + spacing * i, extents.y)
			if direction == UP:
				origin.y = -origin.y
		else:
			origin = Vector2(extents.x, extents.y + spacing * i)
			if direction == LEFT:
				origin.x = -origin.x
		
		var result = space_state.intersect_ray(
			global_position + origin,
			global_position + origin + castTo,
			exceptions,
			mask)
		
		if result:
			buffer[count] = result
			count += 1
	
	return {buffer = buffer, count = count}

func _setBufferSize(value):
	bufferSize = max(value, 2)
	buffer.resize(bufferSize)
