extends "res://Objects/FSM.gd"

# Añadir los posibles estados
func _ready():
	addState("idle")
	addState("run")
	addState("jump")
	addState("fall")
	addState("wallslide")
	call_deferred("setState", states.idle)

func _input(event):
	# Cuando se presiona `jump`
	if event.is_action_pressed("jump"):
		# Si está cayendo, activar el margen de salto (si está en rango)
		if state == states.fall and parent.canBuffer:
			parent.jumpBuffer = true
		
		# Saltar en cualquier estado que lo permita
		if [states.idle, states.run, states.wallslide].has(state):
			parent.jump()
			
			# Tirar el jugador hacia atrás si es un walljump
			if state == states.wallslide:
				# Declaración If ternaria: Es como un if/else, en 1 línea
				# A motion.x se asigna -WALLJUMP si está mirando a la derecha,
				#  o WALLJUMP si la condición no se cumple (facing < 0).
				parent.motion.x = -parent.WALLJUMP if parent.facing > 0 else parent.WALLJUMP

func _stateLogic(delta):
	parent.applyGravity()
	parent.manageMovement()
	parent.showDebug()

# Decidir a qué otro estado se puede pasar desde cada uno
#  (Ver diagrama!)
func _getTransition(delta):
	match state:
		states.idle:
			if not parent.is_on_floor():
				return states.jump if parent.motion.y < 0 else states.fall
			elif abs(parent.motion.x) > 50:
				return states.run
		
		states.run:
			if not parent.is_on_floor():
				return states.jump if parent.motion.y < 0 else states.fall
			elif abs(parent.motion.x) < 50:
				return states.idle
		
		states.jump:
			if parent.canWallslide():
				return states.wallslide
			elif !parent.canWallslide() and parent.motion.y > 0:
				return states.fall
		
		states.fall:
			if parent.is_on_floor():
				return states.idle
			elif parent.motion.y < 0:
				return states.jump
			elif parent.canWallslide():
				return states.wallslide
		
		states.wallslide:
			if parent.is_on_floor():
				return states.idle
			elif !parent.canWallslide() and parent.motion.y > 0:
				return states.fall
			elif !parent.canWallslide() and parent.motion.y < 0:
				return states.jump
	
	return null

func checkJumpBuffer():
	if parent.jumpBuffer:
		parent.jump()
		parent.jumpBuffer = false

# Qué hacer al entrar a cada estado
#  (generalmente aplciar animaciones y reproducir sonidos)
func _enterState(newState, oldState):
	match newState:
		states.idle:
			parent.Sprite.play("Idle")
			checkJumpBuffer()
		states.run:
			parent.Sprite.play("Run")
			checkJumpBuffer()
		states.jump:
			parent.Sprite.play("Jump")
		states.fall:
			parent.Sprite.play("Fall")
		states.wallslide:
			parent.Sprite.flip_h = true if parent.facing == 1 else false
			parent.Sprite.play("Fall")

func _on_JumpArea_body_entered(body):
	if state == states.fall:
		parent.canBuffer = true

func _on_JumpArea_body_exited(body):
	parent.canBuffer = false
