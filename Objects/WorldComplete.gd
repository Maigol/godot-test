#WorldComplete.gd
# Codigo para el ícono de completar el nivel

extends Area2D

# Esto es un ´export´, sirve para agregar una variable que se puede cambiar en
#  cada escena con el editor de escenas en lugar de escribirlo en código
# Este export pide un archivo .tscn
export(String, FILE, "*.tscn") var next_level

# Ir a DeathBarrier.gd para más info
func _physics_process(delta):
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.name == "Player":
			# Cambia la escena al archivo indicado en la variable export
			#  ´next_level´, asignada en cada nivel
			get_tree().change_scene(next_level)
