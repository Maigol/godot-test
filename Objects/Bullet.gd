extends Area2D


const SPEED = 500
const MOTION = Vector2()
onready var Player = get_parent().get_node("Player")
var direction = 1




func _ready():
	direction = Player.facing

func _physics_process(delta):
	MOTION.x = SPEED * direction * delta
	translate(MOTION)

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()



func _on_shoots_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.name != "Player":
		queue_free()
	
